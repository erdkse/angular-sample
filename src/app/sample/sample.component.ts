import {
  Component,
  OnInit,
  OnDestroy,
  Input,
  ChangeDetectionStrategy,
  Output,
  EventEmitter,
  HostListener
} from '@angular/core';

@Component({
  selector: 'app-sample',
  templateUrl: './sample.component.html',
  styleUrls: ['./sample.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SampleComponent implements OnInit, OnDestroy {
  @Input()
  name: string;
  @Output()
  nameChanged: EventEmitter<string> = new EventEmitter<string>();
  @HostListener('mouseover')
  onHover() {
    this.nameChanged.emit('itemHovered');
  }

  constructor() {}

  ngOnInit() {
    console.log('sample init');
  }

  onScrolled(event) {
    event.peventDefault();
  }

  onItemHovered(event) {
    this.nameChanged.emit('itemHovered');
  }

  ngOnDestroy(): void {
    console.log('sample destroyed');
  }
}
