import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private apiURL = 'https://5b603594bde36b00140811b7.mockapi.io/api/v1';

  constructor(private httpClient: HttpClient) {}

  getUsers() {
    return this.httpClient.get<any>(`${this.apiURL}/users`);
  }
}
