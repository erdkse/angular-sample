import {
  Component,
  OnInit,
  OnDestroy,
  AfterViewChecked,
  DoCheck,
  ChangeDetectionStrategy,
  ViewChild,
  AfterViewInit
} from '@angular/core';
import { SampleComponent } from './sample/sample.component';
import { ApiService } from './service/api.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppComponent implements OnInit, DoCheck, AfterViewInit, OnDestroy {
  public title = 'erdi';
  public showSampleComponent = true;
  @ViewChild('sample')
  sample;
  public userList: any[] = [];

  constructor(private apiService: ApiService) {}

  ngOnInit(): void {
    console.log('on init');
    this.apiService.getUsers().subscribe(
      response => {
        console.log('response', response);
        this.userList = response;
      },
      error => {
        console.log('error', error);
      }
    );
    // this.cd.
  }

  ngDoCheck(): void {}

  changeVisibility() {
    this.showSampleComponent = !this.showSampleComponent;
  }

  nameChange($event) {
    console.log('item hovered');
  }

  valueChanged(event) {
    console.log(event.target.value);
    this.title = event.target.value;
  }

  ngAfterViewInit(): void {
    console.log('sata', this.sample.name);
  }

  ngOnDestroy() {
    console.log('on close');
  }
}
